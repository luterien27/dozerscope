from django.db import models

import requests

from django.core.files import File
from django.core.files.temp import NamedTemporaryFile

MAIN_IMAGE_TEMPLATE = "https://i.ytimg.com/vi/%s/mqdefault.jpg"

PLATFORM_CHOICES = (
    ('youtube', 'Youtube'),
)

class Video(models.Model):
    video_id = models.CharField(max_length=30, null=True, blank=True)
    platform = models.CharField(max_length=30, choices=PLATFORM_CHOICES, null=True, blank=True, default="youtube")
    image = models.ImageField(upload_to="images", null=True, blank=True)

    def __unicode__(self):
        return u"%s" % (self.video_id)

    def save(self, *args, **kwargs):
        if not self.image:
            save_image_from_url(self)

        super(Video, self).save(*args, **kwargs)


class VideoComment(models.Model):
    pass


def save_image_from_url(item):

    url = MAIN_IMAGE_TEMPLATE % item.video_id

    r = requests.get(url)

    img_name = item.video_id + ".jpg"

    img_temp = NamedTemporaryFile()
    img_temp.write(r.content)
    img_temp.flush()

    item.image.save(img_name, File(img_temp), save=True)

