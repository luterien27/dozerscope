from django.shortcuts import render

from .models import Video

def home(request, template="home.html"):

	ctx = {
		'videos' : Video.objects.all(),
	}

	return render(request, template, ctx)


