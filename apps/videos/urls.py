from django.conf.urls import *

from .views import home

urlpatterns = patterns('',
	# api - todo: move these to api app
    url(r'^$', 'apps.nostalgia.views.item_details', name="item-detail"),
)
